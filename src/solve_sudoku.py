#solve_sudoku.py

import sys
import argparse
from constants import NINE_DIGITS


class Sudoku:
    ''' Holds a single data structure to represent the sudoku puzzle,
    but has functions to access each view of the puzzle (e.g. the 9th
    quadrant or the 7th row or 8th column). 
    Also any function whose purpose is to add a number to some part
    of the puzzle modifies the single copy of the structure so that
    any subsequence functions that read the data will reflect the
    insertion into the structure.'''

    def __init__(self, str):
        self.lst = str.replace(',', '').replace(' ', '').split('\n')
        
        # Check for newline at end of file (this could be another problem..
        if len(self.lst) > 9:
            self.lst = self.lst[:9]

    def insert(self, col, row, num):
        newRow = self.lst[row]
        newRow = newRow[:col] + num + newRow[(col + 1):]
        self.lst[row] = newRow

    def get_str(self):
        s = ''
        for i in range(len(self.lst)):
            item = self.lst[i]
            for j in range(len(item)):
                s += item[j]
                if ((j + 1) % 3 == 0):
                    s += ' '
            if i == 8:
                break
            
            s += '\n'
            if ((i + 1) % 3 == 0):
                s += '\n'
        return s
        
    def get_num(self, col, row):
        return self.lst[row][col]

    def get_row(self, row):
        rows = []
        strRow = self.lst[row]
        for i in range(9):
            rows.append(strRow[i])
        return rows
        
    def get_col(self, col):
        rows = self.lst
        cols = []
        for i in range(9):
            cols.append(rows[i][col])
        return cols
    
    def get_quad(self, col, row):
        rowRange = get_range(row)
        colRange = get_range(col)

        quad = []
        for c in colRange:
            for r in rowRange:
                quad.append(self.lst[r][c])
        return quad

    def is_solved(self):
        for i in range(9):
            if '0' in self.lst[i]:
                return False
        return True
    
    def solve(self):
        inserted = True
        while (not self.is_solved()):
            if not inserted:
                print "Couldn't insert anywhere,", \
                    "partially solved puzzle is:"
                print self.get_str()
                exit()
                
            inserted = False
            for col in range(9):
                for row in range(9):
                    #TODO Maybe put in a function?
                    allFactors = self.get_row(row) + self.get_col(col) \
                                 + self.get_quad(col, row)
                    allFactors = set(allFactors) # No duplicates allowed
                    if '0' in allFactors:
                        allFactors.remove('0')

                    # Only one digit left in following block:
                    if (len(allFactors) == 8 and \
                        self.get_num(col, row) == '0'):
                        oneNumSet = NINE_DIGITS - allFactors
                        num = oneNumSet.pop()
                        assert len(oneNumSet) == 0
                        self.insert(col, row, num)
                        inserted = True
                    
        print "Completely solved the puzzle! Here it is:"
        print self.get_str()



# Helper functions
def get_range(loc):
    rng = []
    if (loc >= 0 and loc <= 2):
        rng = range(0, 3)
    elif (loc >= 3 and loc <= 5):
        rng = range(3, 6)
    elif (loc >= 6 and loc <= 8):
        rng = range(6, 9)
    else:
	print 'bad loc given to get_range().', \
	       '(needs to be >=0 and <= 8):', loc
	exit()
    return rng





def main():
    parser = argparse.ArgumentParser('solve sudoku puzzles')
    parser.add_argument('file', help='a file that contains a sudoku puzzle')
    args = parser.parse_args()
    fileName = args.file
    
    try:
        file = open(fileName, 'r')
    except IOError:
        print 'File %s does not exist. Exiting.' % fileName 
        exit()

    # Read all the contents of the file.
    strFromFile = file.read()
    sudoku = Sudoku(strFromFile)
    sudoku.solve()


if __name__ == '__main__':
    main()



