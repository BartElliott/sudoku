#test_sudoku.py

import solve_sudoku


def test_rows(sudoku):
    assert sudoku.get_row(0) == list('012345678')
    assert sudoku.get_row(1) == list('abcdefghi')
    assert sudoku.get_row(2) == list('123456789')
    assert sudoku.get_row(3) == list('jklmnopqr')
    assert sudoku.get_row(4) == list('234567890')
    assert sudoku.get_row(5) == list('stuvwxyza')
    assert sudoku.get_row(6) == list('345678901')
    assert sudoku.get_row(7) == list('bcdefghij')
    assert sudoku.get_row(8) == list('456789012')

    print 'test_rows() works.'

def test_cols(sudoku):
    assert sudoku.get_col(0) == list('0a1j2s3b4')
    assert sudoku.get_col(1) == list('1b2k3t4c5')
    assert sudoku.get_col(2) == list('2c3l4u5d6')
    assert sudoku.get_col(3) == list('3d4m5v6e7')
    assert sudoku.get_col(4) == list('4e5n6w7f8')
    assert sudoku.get_col(5) == list('5f6o7x8g9')
    assert sudoku.get_col(6) == list('6g7p8y9h0')
    assert sudoku.get_col(7) == list('7h8q9z0i1')
    assert sudoku.get_col(8) == list('8i9r0a1j2')

    print 'test_cols() works.'

def test_quads(sudoku):
    top_left = sorted(list('012abc123'))
    top_mid = sorted(list('345def456'))
    top_right = sorted(list('678ghi789'))
    mid_left = sorted(list('jkl234stu'))
    mid_mid = sorted(list('mno567vwx'))
    mid_right = sorted(list('pqr890yza'))
    bottom_left = sorted(list('345bcd456'))
    bottom_mid = sorted(list('678efg789'))
    bottom_right = sorted(list('901hij012'))
    
    for r in range(0, 3):
        for c in range(0, 3):
            assert sorted(sudoku.get_quad(c, r)) == top_left
        for c in range(3, 6):
            assert sorted(sudoku.get_quad(c, r)) == top_mid
        for c in range(6, 9):
            assert sorted(sudoku.get_quad(c, r)) == top_right

    for r in range(3, 6):
        for c in range(0, 3):
            assert sorted(sudoku.get_quad(c, r)) == mid_left
        for c in range(3, 6):
            assert sorted(sudoku.get_quad(c, r)) == mid_mid
        for c in range(6, 9):
            assert sorted(sudoku.get_quad(c, r)) == mid_right

    for r in range(6, 9):
        for c in range(0, 3):
            assert sorted(sudoku.get_quad(c, r)) == bottom_left
        for c in range(3, 6):
            assert sorted(sudoku.get_quad(c, r)) == bottom_mid
        for c in range(6, 9):
            assert sorted(sudoku.get_quad(c, r)) == bottom_right

    print 'test_quads() works.'

def test_insert(sudoku):
    pass
    '''print sudoku.get_str()
    sudoku.insert(0, 0, 'x')
    print sudoku.get_str()'''

def test_ranges():
    for i in range(3):
        assert solve_sudoku.get_range(i) == range(3)
    for i in range(3, 6):
        assert solve_sudoku.get_range(i) == range(3, 6)
    for i in range(6, 9):
        assert solve_sudoku.get_range(i) == range(6, 9)
    print 'get_range() works.'


def main():
    file = open('../res/test', 'r')
    strFromFile = file.read()
    sudoku = solve_sudoku.Sudoku(strFromFile)

    test_rows(sudoku)
    test_cols(sudoku)
    test_quads(sudoku)
    test_insert(sudoku)
    test_ranges()


if __name__ == '__main__':
    main()
